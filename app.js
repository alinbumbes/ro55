'use strict'

console.log('hello world')

var myVariable = 23;

var myString2 = 'abc'

var concatVars = myVariable - myString2;

console.log(concatVars)

console.log(typeof NaN)

console.log(0.8 - 0.1)

const myConstant = 45;

// myConstant = 3

let myVarWithLet = 52;
myVarWithLet = 27

console.log(myVarWithLet)


console.log('String')

let var1 = 22;
let var2 = 54;
console.log(var1 + var2)

console.log('Suma dintre ' + var1 + ' si ' + var2 + ' este: ' + '\n'
  + (var1 + var2))

console.log(`Suma dintre
${var1} si              
${var2} este: 
${var1 + var2}`)


// console.log(window)
// Diferenta dintre var, let si const : let si const este block scoped, VAR este function scoped
function Addition(a, b) {
  if (true) {
    var c = 2;
  }

  console.log(c);

  return a + b;
}
console.log(Addition(2, 4));

//tipuri de date primitive
const myNumber = 23;
console.log(typeof myNumber);

const myString = "i am a string";
console.log(typeof myString);

const myBoolean = false;
console.log(typeof myBoolean);

let myUndefined;
console.log(typeof myUndefined);

//tipuri de date by referinta (obiecte)
let myFunc = function patrat(a) {
  return a ** 2;
};

console.log(typeof myFunc);

// if/else   switch - conditie
// while, do while, for - iterative

//aceeasi functie , scrisa in 3 feluri
//function declarations
function fc1(a, b) {
  return a % b;
}

//function expressions
const fc2 = function (a, b) {
  return a % b;
};

//arrow functions
const fc3 = (a, b) => a % b;

// arrays

const zile = ["luni", "marti", "miercuri", "joi", "vineri", 6, 7, false];

console.log(zile[0])
console.log(zile[1])
console.log(zile[2])

for (let i = 0; i < zile.length; i++) {
  console.log(zile[i])
}

for (let zi in zile) {
  console.log(zile[zi])
}

for (let zi of zile) {
  console.log(zi)
}
//iterate through array of arrays
const matrix =
  [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
  ]
const flatMatrix = []

// flatMatrix.push(23, 56)
// console.log(flatMatrix)
for (let i = 0; i < matrix.length; i++) {
  for (let j = 0; j < matrix[i].length; j++) {
    flatMatrix.push(matrix[i][j])
  }
}
console.log(flatMatrix)
//   adaugam noi elemente unui array specificand indexul si valoarea
flatMatrix[10] = "ceva"
console.log(flatMatrix)
// return from flatmatrix all even numbers 

for (let el of flatMatrix) {
  if (el % 2 === 0) console.log(el)
}

const mihai = 17
const iulia = 24

function checkAge(a) {
  if (a >= 18) console.log(`persoana este majora`)
  else console.log(`persoana este minora`)
}
checkAge(mihai)
checkAge(iulia)

const majorCheck = function (e) {
  return e >= 18 ? console.log(` majora`) : console.log(` minora`)
}

const persoane = [['mihai', 17, '23'], ['elena', 25, '69'], ['gigel', 36, '12']]

for (let person of persoane) {
  if ((person[1] >= 18) || Number(person[2]) > 19) {
    console.log(`${person[0]} este  ${person[1] >= 18 ? 'major' : 'minor'}
cu varsta de ${person[1]} si id-ul de facultate ${person[2]}`)
  }
}

console.log(persoane.join('---------------'))

const numePersoane = ['mihai', 'elena', 'gigel', 'Laura', 'Ion', 'Maria', 'Andrei', 'Diana', 'Alex', 'Cristi',]

const numePersoaneStr = numePersoane.join(' ')
console.log(numePersoaneStr)
console.log(typeof numePersoaneStr)
const reConstructed = numePersoaneStr.split(" ")
console.log(reConstructed)
const nr = [2, 5, 7, 23, 21, 8, 11]
nr.sort()
console.log(nr)
console.log(nr.join(" "))
const nr2 = "9 4 7 2"
console.log(nr2)
console.log(nr2.split(" ").map(e => Number(e)).sort().join(" "))



///objects 


const alin = {
  firstName: 'Jonas',
  lastName: 'Schmedtmann',
  age: 2037 - 1991,
  job: 'teacher',
  friends: ['Michael', 'Peter', 'Steven']
};

console.log(alin)

alin.location = 'bucharest'
alin.firstName = 'Alin'
console.log(alin)



for (let items in alin) {
  console.log(`alin are ${items} cu valorile ${alin[items]}  ]}`)
}

for (let items of Object.entries(alin)) {
  console.log(items)
}

let a = 10;
let b = '10'

console.log(a == b)
console.log(a === b)

let c = false;
let d = 'false';
console.log(c == d)
console.log(c === d)

const str1 = 'javascript este un limbaj de programare'

const str2 = 'si grupa 55 nu are nici o legatura cu acest limbaj'

console.log(str1.concat(str2))

console.log(str1.includes('limbaj'))

console.log(str1.slice(str1.indexOf('limbaj') + 6, str1.length))


const myArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

myArr[0] = 100





const ro55 = [
  {

    firstName: 'Jonas',
    lastName: 'Schmedtmann',
    age: 2037 - 1991,
    job: 'teacher',
    friends: ['Michael', 'Peter', 'Steven']
  }
  ,
  {
    firstName: 'ion',
    lastName: 'dolanescu',
    age: 2037 - 1991,
    job: 'singer',
    friends: ['Michael', 'Peter', 'Steven']
  }
  ,
  {
    firstName: 'Florin',
    lastName: 'Piersic',
    age: 2037 - 1921,
    job: 'singer',
    friends: ['Michael', 'Peter', 'Steven']
  }
];
console.log(ro55[2].firstName)

// o functie care primeste ca parametru un array de obiecte si 
// returneaza un console.log cu numele, prenumele si jobul fiecaruia


function printNames(arr) {
  for (let person in arr) {
    console.log(
      `${arr[person].firstName} ${arr[person].lastName} are jobul ${arr[person].job}`
    )
  }
}

printNames(ro55)

const tasks = {
  morning: {
    breakfast: 'eggs',
    emails: 'read them'
  }
  ,
  lunch: {
    food: "cook",
    meetings: ['meeting1', 'meeting2']
  }
}

// iterate through tasks and print all the values
console.log("---------------------------")
for (let task in tasks) {
  console.log(`${task} - ${Object.entries(tasks[task]).join(' ')}`)
}